from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer
import time, os, sys
from threading import Thread
import platform
import json
import base64

if platform.system() == "Windows":
	import win32print, win32ui, win32con
else:
	import subprocess

printerFileList = 'list.txt'
printerList = []
tempFile = 'temp'

class Main(WebSocket):
	def printList(self):
		if(printerList == None):
			self.openPrinterFileList()
		res = []
		if platform.system() == "Windows":
			printersList = win32print.EnumPrinters(win32print.PRINTER_ENUM_LOCAL)
			defPrinter = win32print.GetDefaultPrinter()
			for printer in printersList :
				if(printer[2] == defPrinter):
					res.insert(0, printer[2])
				else:
					res.append(printer[2])
			return res
		else:
			#subprocess.call(['lpstat','-p'])
			res_string = subprocess.check_output(['lpstat','-p'])
			#print res_string
			res_string_lines = res_string.split('\n')
			for lines in res_string_lines:
				if len(lines) > 5:
					printer_name = lines.split()[1]
					res.append(printer_name)
			return res

	def getPrinterStatus(self,printer):
		if platform.system() == "Windows":
			tempPrinter = win32print.OpenPrinter(printer)
			res = win32print.GetPrinter(tempPrinter)
			win32print.ClosePrinter(tempPrinter)
			return res
		else:
			print "Not yet implemented in Linux"
			return None

	def printToPrinter(self,raw_text,printer=None):
		res = {}
		if platform.system() == "Windows":
			print "Print job started"

			if printer == None :
				printer = win32print.GetDefaultPrinter()

			if sys.version_info >= (3,):
				raw_data = bytes(raw_text,"utf-8")
			else:
				raw_data = raw_text

			lines = raw_data.split('\n')

			hPrinter = win32print.OpenPrinter(printer)
			if(self.getPrinterStatus(printer)[18] != 0):
				res['status'] = 500
				res['error'] = 'Printer not available'
				return res
			else:
				try:
					hJob = win32print.StartDocPrinter(hPrinter, 1, ("Print job", None, "RAW"))
					try:
						prin = base64.b64decode(raw_text)
						print prin
						win32print.StartPagePrinter(hPrinter)
						# win32print.WritePrinter(hPrinter, '\x1B\x40') #reset printer
						win32print.WritePrinter(hPrinter, prin)
						# win32print.WritePrinter(hPrinter, '\x0A\x1D\x56\x41\x03')
						win32print.EndPagePrinter(hPrinter)
					except:
						res['status'] = 500
						res['error'] = 'Failed to print'
						return res
					finally:
						win32print.EndDocPrinter(hPrinter)
				except:
					res['status'] = 500
					res['error'] = 'Failed to start printer'
					return res
				finally:
					win32print.ClosePrinter(hPrinter)
					res['status'] = 200
					return res
		else:
			try:
				f = open(tempFile, 'wb')
				raw_data = raw_text
				prin = base64.b64decode(raw_data)
				f.write(prin)
				f.close()

				subprocess.call(['lpr', '-o raw -P ' + printer + ' ' + tempFile])

			except:
				res['status'] = 500
				res['error'] = 'Failed to write temp file'
				return res

	def openPrinterFileList(self):
		try:
			f = open(printerFileList,'r')
			tempList = f.read()
			printerList = json.loads(tempList.decode('utf-8'))
			print printerList
			f.close()
		except:
			f = open(printerFileList,'w')
			tempList = '[]'
			f.write(tempList)
			printerList = []
			f.close()

	def writePrinterFileList(self):
		f = open(printerFileList,'w')
		f.write(json.dumps(printerList))
		f.close()

	def registerPrinter(self, name, ip, printer_array):
		res = {}
		index = 0
		found = False
		while (index < len(printerList)) and (found == False) :
			if(printerList[index]['name'] == name):
				found = True
			else:
				index += 1
		if(found == True):
			printerList[index]['ip'] = ip
			printerList[index]['printers'] = printer_array
		else:
			newPrinter = {}
			newPrinter['name'] = name
			newPrinter['ip'] = ip
			newPrinter['printers'] = printer_array
			printerList.append(newPrinter)
		try:
			self.writePrinterFileList()
			res['status'] = 200
		except:
			print sys.exc_info()[0]
			print sys.exc_info()[1]
			res['status'] = 500
		finally:
			return res

	def responseHandler(self,msg):
		print msg
		param = json.loads(msg.decode('utf-8'))
		res = {}

		if(param['action'] == 'print'):
			res = self.printToPrinter(param['msg'], param['printer'])
			res['action'] = 'print'
			if(res['status'] == 200):
				res['data'] = 'OK'
			else:
				res['data'] = res['error']
		elif(param['action'] == 'list'):
			res['action'] = 'list'
			res['status'] = 200
			res['data'] = self.printList()
		elif(param['action'] == 'get'):
			res['action'] = 'get'
			res['status'] = 200
			res['data'] = self.getPrinterStatus(param['printer'])
		elif(param['action'] == 'register'):
			res = self.registerPrinter(param['name'], param['ip'], param['printer_array'])
			res['action'] = 'register'
		else:
			res['status'] = 500
			res['error'] = "Wrong /unknown command"

		print json.dumps(res)
		self.sendMessage(json.dumps(res))

	def consoleHandler(self):
		exit = False
		while(exit == False):
			command = input('Enter the command : ')
			cmd = command.split()
			if(cmd[0] == 'list'):
				for printer in self.printList():
					print printer
			elif(cmd[0] == 'get'):
				print self.getPrinterStatus(cmd[1])
			elif(cmd[0] == 'exit' or cmd[0] == 'quit'):
				print 'Goodbye'
				exit = True
				sys.exit()

	def handleMessage(self):
		if self.data is None:
			self.data = ''
		else:
			t = Thread(target=self.responseHandler, args=(self.data,))
			t.start()

	def handleConnected(self):
		print self.address, 'connected'

	def handleClose(self):
		print self.address, 'closed'

server = SimpleWebSocketServer('',8000, Main)
server.serveforever()
