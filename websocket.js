var ws_url = 'localhost';
var ws_port = 8000;
var websocket = null;

function onOpen(ev){
  console.log('Connected');
}

function onClose(ev){
  console.log('Disconnected');
}

function onMessage(ev){
  res = JSON.parse(ev.data);
  console.log('Message received : ' + res);

  if(res.action == 'list'){
    if(res.status == 200){
      onListReceived(res.data);
    }
  }else if(res.action == 'get'){

  }else if(res.action == 'print'){

  }else if(res.action == 'register'){
    console.log(res.status);
  }
}

function onError(ev){
  console.log('Error : ' + ev.data);
  websocket.close();
}

function sendMessage(msg){
  console.log('Message sent : ' + msg);
  websocket.send(msg);
}

function connectWebSocket(url, port){
  if(typeof url !== 'undefined'){
    ws_url = url;
  }
  if(typeof port !== 'undefined'){
    ws_port = port;
  }
  console.log('Connecting...');
  websocket = new WebSocket('ws://' + ws_url + ':' + ws_port);
  websocket.onopen = function(ev){onOpen(ev);}
  websocket.onclose = function(ev){onClose(ev);}
  websocket.onmessage = function(ev){onMessage(ev);}
  websocket.onerror = function(ev){onError(ev);}
}

function disconnectWebSocket(){
  console.log('Disconnecting...');
  websocket.close();
}

function setUrl(url){
  ws_url = url;
}

function setPort(port){
  ws_port = port;
}

function getPrinterList(){
  console.log('Getting printer list...');
  cmd = {};
  cmd.action = "list";
  sendMessage(JSON.stringify(cmd));
}

function printText(msg, printer){
  console.log('Printing...');
  cmd = {};
  cmd.action = "print";
  cmd.msg = msg;
  cmd.printer = printer;
  sendMessage(JSON.stringify(cmd));
}

function registerPrinter(name, ip, printer_array){
  console.log('Register printer...');
  cmd = {};
  cmd.action = "register";
  cmd.name = name;
  cmd.ip = ip;
  cmd.printer_array = printer_array;
  sendMessage(JSON.stringify(cmd));
}
