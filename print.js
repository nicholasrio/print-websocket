$(document).ready(function(){
  $('#url_text').val(ws_url);
  $('#port_text').val(ws_port);
  connectWebSocket();
});

$('#list_button').click(function(){
  getPrinterList();
});

$('#disconnect_button').click(function(){
  disconnectWebSocket();
});

$('#connect_button').click(function(){
  connectWebSocket();
})

$('#print_button').click(function(){
  printText($('#message_text').val(), $('#printer_select').val());
});

function onListReceived(list){
  $('#printer_select').find('option').remove().end(); //remove all option first
  list.forEach(function(elm,idx,arr){
    $('#printer_select').append('<option value = "' + elm + '">' + elm + '</option>');
  });
}
